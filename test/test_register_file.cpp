#include <systemc.h>
#include "register_file.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
static const unsigned DATA_OPCODE = 3;
sc_fifo<int> observer_registe_file_dout1;
sc_fifo<int> observer_registe_file_dout2;
int values1[TEST_SIZE]; //vettore usato per tutti i test
int values2[TEST_SIZE]; //vettore usato per tutti i test

SC_MODULE(TestBench) 
{
 	public:
	sc_signal<sc_lv<DATA_OPCODE> > ref_add1;
	sc_signal<sc_lv<DATA_OPCODE> > ref_add2;
	sc_signal<sc_lv<DATA_OPCODE> > ref_add3;
	sc_signal<sc_lv<DATA_WIDTH> > ref_dataw;
	sc_signal<sc_lv<DATA_WIDTH> > ref_dout1;
	sc_signal<sc_lv<DATA_WIDTH> > ref_dout2;
	sc_signal<bool> ref_we; //write enable dataw 	

	Register_file ref1; //REGISTER FILE

  SC_CTOR(TestBench) : ref1("ref1")
  {
 		init_values();

		SC_THREAD(init_values_register_file_test);
		ref1.add1(this->ref_add1);
		ref1.add2(this->ref_add2);
		ref1.add3(this->ref_add3);		
		ref1.dataw(this->ref_dataw);
		ref1.dout1(this->ref_dout1);
		ref1.dout2(this->ref_dout2);
		ref1.we(this->ref_we);
		SC_THREAD(observer_thread_register_file);	
			sensitive << ref1.dout1 << ref1.dout2;	

  }

	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
				
				values2[0] = 1;
				values2[1] = 3;
				values2[2] = 5;
				values2[3] = 7;
				values2[4] = 9;
				values2[5] = 0;
						  		  
	}

	void init_values_register_file_test() {
		ref_we.write(1);
		wait(SC_ZERO_TIME);
		for (unsigned i=0;i<TEST_SIZE;i++) {
			ref_add3.write(values1[i]);
			ref_dataw.write(values2[i]);
			wait(5,SC_NS);
			ref_add1.write(values1[i]);
			ref_add2.write(values1[i]);	
			wait(5,SC_NS);	
		}
	}

	void observer_thread_register_file() {
		while(true) {
				wait();
				int out1 = ref1.dout1->read().to_int();
				int out2 = ref1.dout2->read().to_int();
				cout << "observer_thread: at " << sc_time_stamp() << " reg file dataw: " << ref1.dataw << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " reg file out1: " << out1 << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " reg file out2: " << out2 << "\n\n";
				if (observer_registe_file_dout1.num_free()>0 )  observer_registe_file_dout1.write(out1);
				if (observer_registe_file_dout2.num_free()>0 )  observer_registe_file_dout2.write(out2);
		} 
	}

	int check_register_file() {
		int test1,test2;
		for (unsigned i=0;i<TEST_SIZE;i++) {
				test1=observer_registe_file_dout1.read(); //porta fifo
				test2=observer_registe_file_dout2.read(); //porta fifo
		    if (test1 != values2[i] & test2 != values2[i] )
		        return 1;
		}                  
		return 0;
	}

};

// ---------------------------------------------------------------------------------------


int sc_main(int argc, char* argv[]) {

	TestBench testbench("testbench");
	sc_start();

  return testbench.check_register_file();
}
