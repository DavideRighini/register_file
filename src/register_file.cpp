#include <systemc.h>
#include "register_file.hpp"

#include <fstream>
#include <string>
#include <iostream>


void Register_file::load_file() {
	int i,j;
	char c;
	FILE *inFilePtr;
	char tmp[DATA_WIDTH];
	char const *inFileString = "register_file_start.txt";
	inFilePtr = fopen(inFileString, "r");
	if (inFilePtr == NULL) {
		cout << "Register: error in opening " <<  inFileString << "\n";
		exit (EXIT_FAILURE);
	} else {
		i=0;
		j=DATA_WIDTH-1;
		data[i] = 0;
		do {
			c = fgetc (inFilePtr);
			if (c == '\n' && j == -1) {
				j=DATA_WIDTH-1;
				i++;
			}
			else if (c != EOF && c !=' ') {
				tmp[j]=c;
				data[i].range(j,j) = c;
				j--;
			}
		} while (c != EOF);
		fclose(inFilePtr);
		
		first_load.notify(SC_ZERO_TIME);
	}
}

void Register_file::update_ports() {
	wait(first_load);
  while(true) {
  	wait();
  	wait(2,SC_NS);
		dout1->write(data[add1->read().to_uint()]);
		dout2->write(data[add2->read().to_uint()]);		
	}
}

void Register_file::write_data() {
	while(true) {
		wait(5,SC_NS);
		//Scrittura:
		if (we->read() == 1 & add3->read().to_uint() !=0) {
			data[add3->read().to_uint()] = dataw->read();
		}
	
		//salva contenuto del register_file in un file di testo	
		int i;
		std::string input;
	  std::ofstream out("register_file_end.txt");

		i=0;
		do {
			input = data[i].to_string();
			out << input;				
			out << "\n";
			i++;
		} while (i < NUM_REG);
	  out.close();
	  //------------------------------------------------------
		wait();
	}
}
