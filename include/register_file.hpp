#ifndef REGISTER_FILE_HPP
#define REGISTER_FILE_HPP

SC_MODULE(Register_file) {
	static const unsigned DATA_WIDTH = 16;
	static const unsigned ADDR_WIDTH = 3;
	static const unsigned NUM_REG = 8; //quantità registri
	
	sc_in<sc_lv<ADDR_WIDTH> > add1;
	sc_in<sc_lv<ADDR_WIDTH> > add2;
	sc_in<sc_lv<ADDR_WIDTH> > add3;
	sc_in<sc_lv<DATA_WIDTH> > dataw;
	sc_out<sc_lv<DATA_WIDTH> >dout1;
	sc_out<sc_lv<DATA_WIDTH> >dout2;
	sc_in<bool> we; //write enable dataw
	
	sc_lv<DATA_WIDTH> data[NUM_REG];
	sc_event first_load;
	
	SC_CTOR(Register_file) {
		SC_THREAD(load_file);

		SC_THREAD(update_ports);
			sensitive << add1 << add2;
		
		SC_THREAD(write_data);
			sensitive << we  << add3 << dataw;
	}
	
	private:
	void update_ports();
	void load_file();
	void write_data();
};

#endif
